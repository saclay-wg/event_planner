nix-routes:
	@nix-shell --pure --run "mix phx.routes"

nix-build:  ## Build the project in a pure Nix environment
	@nix-shell --pure --run "make build"

nix-start: ## Start the web application in a pure Nix environment
	@nix-shell --pure --run "make start"

nix-reset: ## Reset the database to a clean state while in a pure Nix environment
	@nix-shell --pure --run "make reset"

nix-test: ## Run the test suite in a pure Nix environment
	@nix-shell --pure --run "mix test"

routes:
	mix phx.routes

build: ## Install dependencies and build the project
	mix deps.get
	mix compile

start: ## Start the web application
	iex -S mix phx.server

reset: ## Reset the database to a clean state
	mix ecto.drop
	mix ecto.create
	mix ecto.migrate
	make seed

test: ## Run the test suite
	mix test

seed: ## Import seed data
	make --directory=priv/repo/seeds dhall
	mix ecto.setup

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.PHONY := help seed reset start build test nix-build nix-start nix-reset nix-test

.DEFAULT_GOAL := help
