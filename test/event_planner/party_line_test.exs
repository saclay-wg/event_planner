defmodule EventPlanner.PartyLineTest do
  use EventPlanner.DataCase

  alias EventPlanner.PartyLine

  describe "groups" do
    alias EventPlanner.PartyLine.Group

    @valid_attrs %{}
    @update_attrs %{}
    @invalid_attrs %{}

    def group_fixture(attrs \\ %{}) do
      {:ok, group} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PartyLine.create_group()

      group
    end

    test "list_groups/0 returns all groups" do
      group = group_fixture()
      assert PartyLine.list_groups() == [group]
    end

    test "get_group!/1 returns the group with given id" do
      group = group_fixture()
      assert PartyLine.get_group!(group.id) == group
    end

    test "create_group/1 with valid data creates a group" do
      assert {:ok, %Group{} = group} = PartyLine.create_group(@valid_attrs)
    end

    test "create_group/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = PartyLine.create_group(@invalid_attrs)
    end

    test "update_group/2 with valid data updates the group" do
      group = group_fixture()
      assert {:ok, %Group{} = group} = PartyLine.update_group(group, @update_attrs)
    end

    test "update_group/2 with invalid data returns error changeset" do
      group = group_fixture()
      assert {:error, %Ecto.Changeset{}} = PartyLine.update_group(group, @invalid_attrs)
      assert group == PartyLine.get_group!(group.id)
    end

    test "delete_group/1 deletes the group" do
      group = group_fixture()
      assert {:ok, %Group{}} = PartyLine.delete_group(group)
      assert_raise Ecto.NoResultsError, fn -> PartyLine.get_group!(group.id) end
    end

    test "change_group/1 returns a group changeset" do
      group = group_fixture()
      assert %Ecto.Changeset{} = PartyLine.change_group(group)
    end
  end

  describe "groups" do
    alias EventPlanner.PartyLine.Group

    @valid_attrs %{displayName: "some displayName", email: "some email", node_id: "some node_id", photo: "some photo"}
    @update_attrs %{displayName: "some updated displayName", email: "some updated email", node_id: "some updated node_id", photo: "some updated photo"}
    @invalid_attrs %{displayName: nil, email: nil, node_id: nil, photo: nil}

    def group_fixture(attrs \\ %{}) do
      {:ok, group} =
        attrs
        |> Enum.into(@valid_attrs)
        |> PartyLine.create_group()

      group
    end

    test "list_groups/0 returns all groups" do
      group = group_fixture()
      assert PartyLine.list_groups() == [group]
    end

    test "get_group!/1 returns the group with given id" do
      group = group_fixture()
      assert PartyLine.get_group!(group.id) == group
    end

    test "create_group/1 with valid data creates a group" do
      assert {:ok, %Group{} = group} = PartyLine.create_group(@valid_attrs)
      assert group.displayName == "some displayName"
      assert group.email == "some email"
      assert group.node_id == "some node_id"
      assert group.photo == "some photo"
    end

    test "create_group/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = PartyLine.create_group(@invalid_attrs)
    end

    test "update_group/2 with valid data updates the group" do
      group = group_fixture()
      assert {:ok, %Group{} = group} = PartyLine.update_group(group, @update_attrs)
      assert group.displayName == "some updated displayName"
      assert group.email == "some updated email"
      assert group.node_id == "some updated node_id"
      assert group.photo == "some updated photo"
    end

    test "update_group/2 with invalid data returns error changeset" do
      group = group_fixture()
      assert {:error, %Ecto.Changeset{}} = PartyLine.update_group(group, @invalid_attrs)
      assert group == PartyLine.get_group!(group.id)
    end

    test "delete_group/1 deletes the group" do
      group = group_fixture()
      assert {:ok, %Group{}} = PartyLine.delete_group(group)
      assert_raise Ecto.NoResultsError, fn -> PartyLine.get_group!(group.id) end
    end

    test "change_group/1 returns a group changeset" do
      group = group_fixture()
      assert %Ecto.Changeset{} = PartyLine.change_group(group)
    end
  end

  test "serialise" do
  event_db = %EventPlanner.PartyLine.Event{
    author_id: "06f4e0e1-ed83-4499-9374-b29b8ee3b9aa",
    author_type: :user,
    authors_node_id: "polytechnique",
    description: "À vos sacs poubelles ! Prêt⋅e⋅s ! Partez !!",
    endTime: "2019-04-13T16:25:25Z",
    hash: "481383601CB52A6A95D25FEC8FEB16E8125B1AF8C1FEF60A8BA51EAAA1D7BEFA",
    inserted_at: ~N[2019-06-06 15:56:16],
    place: "828 Boulevard des Maréchaux, 91120 Palaiseau, France",
    startTime: "2019-04-13T14:25:25Z",
    title: "Soirée ramassage de déchets",
    updated_at: ~N[2019-06-06 15:56:16],
  }

  event_api = %{
    author: %{
    id: "06f4e0e1-ed83-4499-9374-b29b8ee3b9aa",
    node_id: "polytechnique",
    type: :user
  },
  description: "À vos sacs poubelles ! Prêt⋅e⋅s ! Partez !!",
  endTime: "2019-04-13T16:25:25Z",
  place: "828 Boulevard des Maréchaux, 91120 Palaiseau, France",
  startTime: "2019-04-13T14:25:25Z",
  title: "Soirée ramassage de déchets",
  uuid: "8779e9c7-0110-475f-975b-e81a38aac509"
}

    assert serialise_event(event_db, to: :api) == serialise_event(event_api, to: :db)

  end
end
