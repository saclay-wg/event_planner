defmodule EventPlanner.PartyLine do
  @moduledoc """
  The PartyLine context.
  """

  import Ecto.Query, warn: false
  require Logger
  alias EventPlanner.Repo

  alias EventPlanner.PartyLine.{User, Event, Post, Group}
  import EventPlannerWeb.Plugs.UniquePayloads, only: [hash_payload: 1]


  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user!(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert!()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a User.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{source: %User{}}

  """
  def change_user(%User{} = user) do
    User.changeset(user, %{})
  end


  def process_event_params(user, params) when is_map(params) do
    %{description: params.description,
      startTime: params.startTime,
      endTime: params.endTime,
      place: params.place,
      title: params.title,
      node_id: params.node_id,
      author: %{id: user.id, email: user.email, node_id: user.node_id, type: "user"}
    }
  end

  # FIXME: Put back hashing
  def create_event!(user, attrs \\ %{}) do
    attrs = process_event_params(user, attrs)

    user
    |> Ecto.build_assoc(:events)
    |> Event.changeset(attrs)
    |> Repo.insert!
  end

  def create_event(attrs \\ %{}) do
    try do
      r = create_event!(attrs)
      {:ok, r}
    rescue
      e -> {:error, e}
    end
  end

  def get_event!(id) do
    Event
    |> Repo.get!(id)
    |> Repo.preload(:children)
  end

  def list_events() do
    Event
    |> Repo.all
    |> Repo.preload(:children)
  end

  def delete_event(event) do
    Repo.delete(event)
  end

  def update_event(%Event{} = event, attrs) do
    event
    |> Event.changeset(attrs)
    |> Repo.update()
  end

  def serialise_event(event, to: :db) when is_map(event) do
    %{author_id: event.author.uuid,
      authors_node_id: event.author.node_id,
      author_type: event.author.type,
      description: event.description,
      endTime: event.endTime,
      place: event.place,
      startTime: event.startTime,
      title: event.title,
    }
  end

  def process_post_params(user, params) when is_map(params) do
    %{title: params.title, description: params.description,
      node_id: params.node_id, attachments: params.attachments,
      author: %{id: user.id, node_id: user.node_id, type: "user"}
    }
  end

  #FIXME: put back the hash
  def create_post!(user, attrs) do
    attrs = process_post_params(user, attrs)
    user
    |> Ecto.build_assoc(:posts)
    |> Post.changeset(attrs)
    |> Repo.insert!
  end

  def create_post!(attrs) do
    Post.changeset(%Post{}, attrs)
    |> Repo.insert!
    |> Repo.preload([:children, :post, :event])
  end

  def create_post(user, attrs \\ %{}) do
    try do
      r = create_post!(user, attrs)
      {:ok, r}
    rescue
      e -> {:error, e}
    end
  end

  def get_post!(id) do
    Post
    |> Repo.get!(id)
    |> Repo.preload([:post, :event, :children])
  end

  def get_post(id) do
    case get_post!(id) do
      nil -> {:error, :noresult}
      res -> {:ok, res}
    end
  end

  def list_posts() do
    Repo.all(Post)
  end

  def delete_post(post) do
    Repo.delete(post)
  end

  def update_post(%Post{} = post, attrs) do
    post
    |> Post.changeset(attrs)
    |> Repo.update()
  end

  #FIXME: put back the hash
  def create_response(%{attrs: attrs, parent: parent, user: user, to: :post}) do
    attrs = process_post_params(user, attrs)
    %Post{post_id: parent.id, user_id: user.id}
           |> Post.changeset(attrs)
           |> Repo.insert!
  end

  def create_response(%{attrs: attrs, parent: parent, to: :post}) do
    attrs = process_post_params(attrs.author, attrs)
    %Post{post_id: parent.id}
      |> Post.changeset(attrs)
      |> Repo.insert!
      |> Repo.preload([:children, :post, :event])
  end

  def create_response(%{attrs: attrs, parent: parent, user: user, to: :event}) do
    attrs = process_post_params(user, attrs)
    post = %Post{event_id: parent.id, user_id: user.id}
           |> Post.changeset(attrs)
           |> Repo.insert!
    post
    |> Post.changeset(attrs)
    |> Repo.update!
    post
  end

  @doc """
  Returns the list of groups.

  ## Examples

      iex> list_groups()
      [%Group{}, ...]

  """
  def list_groups do
    Repo.all(Group)
  end

  @doc """
  Gets a single group.

  Raises `Ecto.NoResultsError` if the Group does not exist.

  ## Examples

      iex> get_group!(123)
      %Group{}

      iex> get_group!(456)
      ** (Ecto.NoResultsError)

  """
  def get_group!(id), do: Repo.get!(Group, id)

  @doc """
  Creates a group.

  ## Examples

      iex> create_group(%{field: value})
      {:ok, %Group{}}

      iex> create_group(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_group(attrs \\ %{}) do
    %Group{}
    |> Group.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a group.

  ## Examples

      iex> update_group(group, %{field: new_value})
      {:ok, %Group{}}

      iex> update_group(group, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_group(%Group{} = group, attrs) do
    group
    |> Group.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Group.

  ## Examples

      iex> delete_group(group)
      {:ok, %Group{}}

      iex> delete_group(group)
      {:error, %Ecto.Changeset{}}

  """
  def delete_group(%Group{} = group) do
    Repo.delete(group)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking group changes.

  ## Examples

      iex> change_group(group)
      %Ecto.Changeset{source: %Group{}}

  """
  def change_group(%Group{} = group) do
    Group.changeset(group, %{})
  end
end
