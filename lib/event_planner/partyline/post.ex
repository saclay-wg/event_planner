defmodule EventPlanner.PartyLine.Post do
  alias __MODULE__
  use Ecto.Schema
  import Ecto.Changeset
  alias EventPlanner.PartyLine.{Post, User, Event, Author}

  @attrs [:title, :attachments, :description, :node_id, :hash]
  @required_attrs @attrs -- [:title, :children]

  @derive {Jason.Encoder, only: [:attachments, :author, :created_at, :updated_at, :description, :node_id, :uuid, :title]}
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "posts" do
    field :attachments, {:array, :string}
    field :description, :string
    field :hash, :string
    field :node_id, :string
    field :title, :string

    has_many :children, Post
    belongs_to :user, User
    belongs_to :post, Post
    belongs_to :event, Event
    embeds_one :author, Author

    timestamps(inserted_at: :created_at)
  end

  @doc false
  def changeset(post, attrs) do
    post
    |> cast(attrs, [:title, :node_id, :attachments, :description, :hash])
    |> validate_required([:node_id, :attachments, :description])
    |> cast_assoc(:user)
    |> cast_assoc(:event)
    |> cast_assoc(:post)
    |> cast_embed(:author)
  end
end
