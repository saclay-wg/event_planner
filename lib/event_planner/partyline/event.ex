defmodule EventPlanner.PartyLine.Event do
  use Ecto.Schema
  import Ecto.Changeset
  alias EventPlanner.PartyLine.{Post, Author, User}

  @attrs [:description, :startTime, :endTime, :place, :title, :node_id]
  @derive {Jason.Encoder, only: @attrs -- [:hash]}
  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "events" do
    field :description, :string
    field :startTime, :utc_datetime
    field :endTime, :utc_datetime
    field :place, :string
    field :title, :string
    field :node_id, :string

    field :hash, :string
    has_many :children, Post
    embeds_one :author, Author
    belongs_to :user, User

    timestamps(inserted_at: :created_at)
  end

  @doc false
  def changeset(event, attrs) do
    event
    |> cast(attrs, @attrs)
    |> validate_required(@attrs)
    |> cast_assoc(:user)
    |> cast_embed(:author)
  end
end
