defmodule EventPlanner.PartyLine.Parent do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: false}
  schema "parents" do
    field :type, :string
  end

  def changeset(parent, attrs) do
    parent
    |> cast(attrs, [:id, :type])
    |> validate_required([:id, :type])
  end
end
