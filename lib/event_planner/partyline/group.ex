defmodule EventPlanner.PartyLine.Group do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:uuid, Ecto.UUID, autogenerate: true}
  @foreign_key_type Ecto.UUID
  schema "groups" do
    field :displayName, :string
    field :email, :string
    field :node_id, :string
    field :photo, :string

    timestamps()
  end

  @doc false
  def changeset(group, attrs) do
    group
    |> cast(attrs, [:displayName, :email, :photo, :node_id])
    |> validate_required([:displayName, :email, :photo, :node_id])
  end
end
