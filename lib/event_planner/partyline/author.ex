defmodule EventPlanner.PartyLine.Author do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, :binary_id, autogenerate: false}
  @foreign_key_type :binary_id
  schema "authors" do
    field :node_id, :string
    field :type, :string
  end

  @doc false
  def changeset(author, attrs) do
    author
    |> cast(attrs, [:node_id, :type, :id])
    |> validate_required([:node_id, :type, :id])
  end
end

