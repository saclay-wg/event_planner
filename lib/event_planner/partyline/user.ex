defmodule EventPlanner.PartyLine.User do
  use Ecto.Schema
  import Ecto.Changeset
  alias EventPlanner.PartyLine.{Post, Event}

  @attrs [:displayName, :email, :photo, :node_id]

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  @derive {Jason.Encoder, only: @attrs}

  schema "users" do
    field :displayName, :string
    field :email,  :string
    field :photo,  :string
    field :node_id, :string

    has_many :posts, Post
    has_many :events, Event

    timestamps(inserted_at: :created_at)
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, @attrs)
    |> validate_required(@attrs)
    |> unique_constraint(:email)
  end


  defimpl Enumerable, for: __MODULE__ do
    def count(map) do
      {:ok, map_size(map)}
    end

    def member?(map, {key, value}) do
      {:ok, match?(%{^key => ^value}, map)}
    end

    def member?(_map, _other) do
      {:ok, false}
    end

    def slice(map) do
      {:ok, map_size(map), &Enumerable.List.slice(:maps.to_list(map), &1, &2)}
    end

    def reduce(map, acc, fun) do
      reduce_list(:maps.to_list(map), acc, fun)
    end

    defp reduce_list(_list, {:halt, acc}, _fun), do: {:halted, acc}
    defp reduce_list(list, {:suspend, acc}, fun), do: {:suspended, acc, &reduce_list(list, &1, fun)}
    defp reduce_list([], {:cont, acc}, _fun), do: {:done, acc}
    defp reduce_list([head | tail], {:cont, acc}, fun), do: reduce_list(tail, fun.(head, acc), fun)
  end
end
