defmodule EventPlannerWeb.Plugs.UniquePayloads do

  import Plug.Conn
  alias EventPlannerWeb.Router.Helpers, as: Routes

  def init(opts), do: opts

  def call(%{params: %{parent: _parent}=post}=conn, _) do
    payload_hash = hash_payload(post)
    case Cachex.get(:post_hashes, payload_hash) do
      {:ok, nil}  -> conn
      {:ok, uuid} ->
        location = Routes.post_url(conn, :show, uuid)
        conn
        |> put_resp_header("Location", location)
        |> send_resp(:conflict, "")
        |> halt()
    end
  end

  def call(%{params: event}=conn ,_) do
    payload_hash = hash_payload(event)
    case Cachex.get(:event_hashes, payload_hash) do
      {:ok, nil}  -> conn
      {:ok, uuid} ->
        location = Routes.event_url(conn, :show, uuid)
        conn
        |> put_resp_header("Location", location)
        |> send_resp(:conflict, "")
        |> halt()
    end
  end

  def hash_payload(payload) when is_map(payload) do
    
    :crypto.hash(:sha256, Jason.encode!(payload))
    |> Base.encode16
  end
end
