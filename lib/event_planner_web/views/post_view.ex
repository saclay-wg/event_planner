defmodule EventPlannerWeb.PostView do
  use EventPlannerWeb, :view
  alias EventPlannerWeb.PostView

  def render("index.json", %{posts: posts}) do
    render_many(posts, PostView, "post.json")
  end

  def render("show.json", %{post: post}) do
    render_one(post, PostView, "post.json")
  end

  def render("post.json", %{post: post}) do
    %{
      attachments: post.attachments,
      author: %{
        uuid: post.author.id,
        nodeId: post.author.node_id,
        type: post.author.type,
      },
      children: render_alone(post.children),
      createdAt: post.created_at,
      description: post.description,
      nodeId: post.node_id,
      parent: render_parent(post),
      title: post.title,
      updatedAt: post.updated_at,
      uuid: post.id,
    }
  end

  def render_parent(post) do
    cond do
      post.post  -> %{id: post.post_id, type: "post"}
      post.event -> %{id: post.event_id, type: "event"}
      true       -> nil
    end
  end

  def render_alone(children) when is_list(children) do
    Enum.map(children, fn child -> 
      %{
        attachments: child.attachments,
        author: %{
          uuid: child.author.id,
          nodeId: child.author.node_id,
          type: child.author.type,
        },
        children: [],
        createdAt: child.created_at,
        description: child.description,
        nodeId: child.node_id,
        parent: render_parent(child),
        title: child.title,
        updateAt: child.updated_at,
        uuid: child.id
      }
    end)
  end
end
