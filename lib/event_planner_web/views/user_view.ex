defmodule EventPlannerWeb.UserView do
  use EventPlannerWeb, :view
  alias EventPlannerWeb.UserView

  def render("index.json", %{users: users}) do
    [render_many(users, UserView, "user.json")]
  end

  def render("show.json", %{user: user}) do
    render_one(user, UserView, "user.json")
  end

  def render("user.json", %{user: user}) do
    user
  end
end
