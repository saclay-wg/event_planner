defmodule EventPlannerWeb.GroupView do
  use EventPlannerWeb, :view
  alias EventPlannerWeb.GroupView

  def render("index.json", %{groups: groups}) do
    render_many(groups, GroupView, "group.json")
  end

  def render("show.json", %{group: group}) do
    render_one(group, GroupView, "group.json")
  end

  def render("group.json", %{group: group}) do
    %{id: group.uuid,
      displayName: group.displayName,
      email: group.email,
      photo: group.photo,
      node_id: group.node_id,
      uuid: group.uuid
    }

  end
end
