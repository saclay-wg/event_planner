defmodule EventPlannerWeb.EventView do
  use EventPlannerWeb, :view
  alias EventPlannerWeb.EventView
  import EventPlanner.PartyLine

  def render("index.json", %{events: events}) do
    render_many(events, EventView, "event.json")
  end

  def render("show.json", %{event: event}) do
    render_one(event, EventView, "event.json")
  end

  def render("event.json", %{event: event}) do
    %{
      author: %{
        uuid: event.author.id,
        nodeId: event.author.node_id,
        type: event.author.type,
      },
      description: event.description,
      children: render_alone(event.children),
      endTime: event.endTime,
      place: event.place,
      startTime: event.startTime,
      title: event.title,
      uuid: event.id,
      nodeId: event.node_id
    }
  end

  def render_alone(children) when is_list(children) do
    Enum.map(children, fn child -> 
      %{
        author: %{
          uuid: child.author.id,
          nodeId: child.author.node_id,
          type: child.author.type,
        },
        children: [],
        createdAt: child.created_at,
        description: child.description,
        nodeId: child.node_id,
        parent: %{id: child.event_id, type: "event"},
        title: child.title,
        updateAt: child.updated_at,
        uuid: child.id
      }
    end)
  end
end
