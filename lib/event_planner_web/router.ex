defmodule EventPlannerWeb.Router do
  use EventPlannerWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/partyline", EventPlannerWeb do
    pipe_through :api

    get "/user/:uuid",            UserController,  :show
    get "/group/:uuid",           GroupController, :show

    get "/posts",                 PostController, :index
    delete "/posts/:uuid",        PostController, :delete
    patch "/posts/:uuid",         PostController, :update
    get "/posts/:uuid",           PostController, :show
    post "/posts/new",            PostController, :create
    post "/posts/respond/:uuid",  PostController, :respond

    get "/events",                EventController, :index
    get "/events/:uuid",          EventController, :show
    post "/events/new",           EventController, :create
    post "/events/respond/:uuid", EventController, :respond
    delete "/events/:uuid",       EventController, :delete
    patch "/events/edit",         EventController, :update
  end

  # scope "/node", EventPlannerWeb do

  # end
end
