defmodule EventPlannerWeb.EventController do
  use EventPlannerWeb, :controller

  alias EventPlanner.PartyLine
  alias EventPlanner.PartyLine.Event
  alias EventPlannerWeb.Plugs.UniquePayloads

  plug UniquePayloads when action in [:create]

  action_fallback EventPlannerWeb.FallbackController

  def index(conn, _params) do
    events = PartyLine.list_events()
    render(conn, "index.json", events: events)
  end

  def create(conn, event_params) do
    atomised_params = AtomicMap.convert(event_params, underscore: false)
    with {:ok, %Event{} = event} <- PartyLine.create_event(atomised_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.event_path(conn, :show, event))
      |> render("show.json", event: event)
    end
  end

  def show(conn, %{"uuid" => uuid}) do
    event = PartyLine.get_event!(uuid)
    render(conn, "show.json", event: event)
  end

  # def update(conn, params) do
  #   event = PartyLine.get_event!(id)

  #   with {:ok, %Event{} = event} <- PartyLine.update_event(event, event_params) do
  #     render(conn, "show.json", event: event)
  #   end
  # end

  def delete(conn, %{"uuid" => uuid}) do
    event = PartyLine.get_event!(uuid)

    with {:ok, %Event{}} <- PartyLine.delete_event(event) do
      send_resp(conn, :no_content, "")
    end
  end
end
