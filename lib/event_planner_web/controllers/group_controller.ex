defmodule EventPlannerWeb.GroupController do
  use EventPlannerWeb, :controller

  alias EventPlanner.PartyLine
  alias EventPlanner.PartyLine.Group

  action_fallback EventPlannerWeb.FallbackController

  def index(conn, _params) do
    groups = PartyLine.list_groups()
    render(conn, "index.json", groups: groups)
  end

  def create(conn, %{"group" => group_params}) do
    with {:ok, %Group{} = group} <- PartyLine.create_group(group_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.group_path(conn, :show, group))
      |> render("show.json", group: group)
    end
  end

  def show(conn, %{"uuid" => id}) do
    group = PartyLine.get_group!(id)
    render(conn, "show.json", group: group)
  end

  def update(conn, %{"id" => id, "group" => group_params}) do
    group = PartyLine.get_group!(id)

    with {:ok, %Group{} = group} <- PartyLine.update_group(group, group_params) do
      render(conn, "show.json", group: group)
    end
  end

  def delete(conn, %{"id" => id}) do
    group = PartyLine.get_group!(id)

    with {:ok, %Group{}} <- PartyLine.delete_group(group) do
      send_resp(conn, :no_content, "")
    end
  end
end
