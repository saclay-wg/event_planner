defmodule EventPlannerWeb.UserController do
  use EventPlannerWeb, :controller

  alias EventPlanner.PartyLine
  alias EventPlanner.PartyLine.User

  action_fallback EventPlannerWeb.FallbackController

  def show(conn, %{"uuid" => id}) do
    user = PartyLine.get_user!(id)
    render(conn, "show.json", user: user)
  end
end
