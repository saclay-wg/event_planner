defmodule EventPlannerWeb.PostController do
  use EventPlannerWeb, :controller
  alias EventPlanner.PartyLine
  alias EventPlanner.PartyLine.Post
  alias EventPlannerWeb.Plugs.UniquePayloads

  plug UniquePayloads when action in [:create]

  action_fallback EventPlannerWeb.FallbackController

  def index(conn, _params) do
    posts = PartyLine.list_posts()
    render(conn, "index.json", posts: posts)
  end

  def create(conn, post_params) do
    params = AtomicMap.convert(post_params)
    case PartyLine.create_post!(params) do
      %Post{}=post ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.post_url(conn, :show, post))
        |> render("show.json", post: post)
      e ->
        IO.inspect(e)
        conn
        |> resp(500, "Internal server error")
        |> send_resp()
    end
  end

  def respond(conn, post_params) do
    params = AtomicMap.convert(post_params)
    parent = %{type: :post, id: conn.path_params["uuid"]}
    case PartyLine.create_response(%{parent: parent, attrs: params, to: :post}) do
      %Post{}=post ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", Routes.post_url(conn, :show, post))
        |> render("show.json", post: post)
      e ->
        IO.inspect(e)
        conn
        |> resp(500, "Internal server error")
        |> send_resp()
    end
  end

  def show(conn, %{"uuid" => uuid}) do
    case PartyLine.get_post(uuid) do
      {:ok, post}         -> render(conn, "show.json", post: post)
      {:error, :noresult} -> conn |> resp(404, "Not found") |> send_resp()
    end
  end

  def update(conn, %{"uuid" => uuid, post: post_params}) do
    params = AtomicMap.convert(post_params)
    post = PartyLine.get_post!(uuid)

    with {:ok, %Post{} = post} <- PartyLine.update_post(post, params) do
      render(conn, "show.json", post: post)
    end
  end

  def delete(conn, %{"uuid" => uuid}) do
    post = PartyLine.get_post!(uuid)

    with {:ok, %Post{}} <- PartyLine.delete_post(post) do
      send_resp(conn, :no_content, "")
    end
  end
end
