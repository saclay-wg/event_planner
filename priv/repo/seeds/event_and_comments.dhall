let Types = ./types.dhall

let author1 =
        { type = "user"
        , email = "olivér.facklam@polytechnique.edu"
        , node_id = "polytechnique"
        , id = "00000000-0000-0000-0000-000000000001"
        }
      : Types.Author

let commenter1 =
        { id = "00000000-0000-0000-0000-000000000003"
        , node_id = "ens-paris"
        , email = "s.dacunha@crans.org"
        , type = "user"
        }
      : Types.Author

let commenter2 =
        { id = "00000000-0000-0000-0000-000000000005"
        , node_id = "ensta"
        , email = "lac.thi.cuc.gam@data-ensta.fr"
        , type = "user"
        }
      : Types.Author

let commenter3 =
        { id = "00000000-0000-0000-0000-000000000004"
        , node_id = "telecom-paris"
        , email = "gudhfinna.sigfusdottir@rezel.net"
        , type = "user"
        }
      : Types.Author

let original_event =
        { startTime =
            "2020-06-30T08:30:00.000Z"
        , endTime = "2020-06-30T10:00:00.000Z"
        , node_id = "polytechnique"
        , author = author1
        , title = "Petit-déjeuner sur la pelouse sous le mat !"
        , place = "48.711374,2.210961"
        , description =
            ''
            Le petit-déjeuner d'été 2020 est officiellement là !
            Notre rendez-vous inter-écoles se déroulera sur la pelouse du mat de l'X, à partir de
            8h30. Ramenez de quoi manger et boire, et n'oubliez pas votre bonne humeur !
            ''
        }
      : Types.Event

let comment01 =
        { title = ""
        , description = "Il y aura des prises pour les bouilloires ?"
        , createdAt = "2019-09-30T13:30:00.000Z"
        , updatedAt = "2019-09-30T13:30:00.000Z"
        , node_id = "polytechnique"
        , author = commenter1
        , attachments = [] : List Text
        }
      : Types.Post

let comment02 =
        { title = ""
        , description = "Wéééé génial !!"
        , createdAt = "2019-09-30T13:50:00.000Z"
        , updatedAt = "2019-09-30T14:10:00.000Z"
        , node_id = "polytechnique"
        , author = commenter2
        , attachments = [] : List Text
        }
      : Types.Post

let subcomment01 =
        { title =
            ""
        , description =
            "Hahahaha tu crois qu'on va faire descendre une multiprise depuis les BE ?"
        , createdAt = "2019-09-30T13:50:00.000Z"
        , updatedAt = "2019-09-30T13:50:00.000Z"
        , node_id = "polytechnique"
        , author = commenter1
        , attachments = [] : List Text
        }
      : Types.Post

let subsubcomment01 =
        { title = ""
        , description = "Tu ne crois pas si bien dire…"
        , createdAt = "2019-09-30T13:50:00.000Z"
        , updatedAt = "2019-09-30T13:50:00.000Z"
        , node_id = "polytechnique"
        , author = commenter2
        , attachments = [] : List Text
        }
      : Types.Post

in  { event = original_event
    , comments = [ comment01, comment02 ]
    , subcomments = [ subcomment01 ]
    , subsubcomments = [ subsubcomment01 ]
    }
