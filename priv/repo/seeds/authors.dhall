let authors =
      [ { name = "Olivér Facklam"
        , email = "olivér.facklam@polytechnique.edu"
        , node_id = "polytechnique"
        , id = "00000000-0000-0000-0000-000000000001"
        }
      , { name = "Wen Liu"
        , email = "wen.liu@institutoptique.fr"
        , node_id = "supoptique"
        , id = "00000000-0000-0000-0000-00000000002"
        }
      , { name = "Stéphanie da Cunha"
        , email = "s.dacunha@crans.org"
        , node_id = "ens-paris"
        , id = "00000000-0000-0000-0000-000000000003"
        }
      , { name = "Guðfinna Sigfúsdóttir"
        , email = "gudhfinna.sigfusdottir@rezel.net"
        , node_id = "telecom-paris"
        , id = "00000000-0000-0000-0000-000000000004"
        }
      , { name = "Lạc Thị Cúc Gấm"
        , email = "lac.thi.cuc.gam@data-ensta.fr"
        , node_id = "ensta"
        , id = "00000000-0000-0000-0000-000000000005"
        }
      , { name = "Praxidike Meng"
        , email = "p.meng@viarezo.fr"
        , node_id = "centrale-supelec"
        , id = "00000000-0000-0000-0000-000000000006"
        }
      ]

in  authors
