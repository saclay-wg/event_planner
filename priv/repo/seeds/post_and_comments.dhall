let Types = ./types.dhall


let original_author =
        { id = "00000000-0000-0000-0000-000000000004"
        , node_id = "telecom-paris"
        , email = "gudhfinna.sigfusdottir@rezel.net"
        , type = "user"
        }
      : Types.Author

let author2 =
        { id = "00000000-0000-0000-0000-000000000005"
        , node_id = "ensta"
        , email = "lac.thi.cuc.gam@data-ensta.fr"
        , type = "user"
        }
      : Types.Author

let author3 =
        { id = "00000000-0000-0000-0000-000000000003"
        , node_id = "ens-cachan"
        , email = "s.dacunha@crans.org"
        , type = "user"
        }
      : Types.Author

let original_post =
        { title = "Est-ce que quelqu'un a vu un collier de perles ?"
        , description =
            ''
            J'ai perdu mon collier de perles dans le parc près de l'X…
            Est-ce que quelqu'un l'aurait vu ?
            ''
        , createdAt = "2020-05-28T22:30:00.000Z"
        , updatedAt = "2020-05-28T22:30:00.000Z"
        , node_id = "polytechnique"
        , author = original_author
        , attachments = [] : List Text
        }
      : Types.Post

let comment01 =
        { title = ""
        , description = "Non, désolé :/"
        , createdAt = "2020-05-28T22:35:00.000Z"
        , updatedAt = "2020-05-28T22:36:00.000Z"
        , node_id = "polytechnique"
        , author = author2
        , attachments = [] : List Text
        }
      : Types.Post

let comment02 =
        { title =
            ""
        , description =
            "Est-ce qu'il est bleu et blanc ? J'en ai vu un traîner vers l'arrêt de bus"
        , createdAt = "2020-05-28T22:37:00.000Z"
        , updatedAt = "2020-05-28T22:37:00.000Z"
        , node_id = "polytechnique"
        , author = author3
        , attachments = [ "https://i.imgur.com/D1nITb9.png" ]
        }
      : Types.Post

let comment03 =
        { title = ""
        , description = "Merciiiii ❤️"
        , createdAt = "2020-05-28T23:03:00.000Z"
        , updatedAt = "2020-05-28T23:03:00.000Z"
        , node_id = "polytechnique"
        , author = original_author
        , attachments = [ "https://i.imgur.com/dfoj4AD.jpg" ]
        }
      : Types.Post

in  { post = original_post
    , comments = [ comment01, comment02 ]
    , subcomment = comment03
    }
