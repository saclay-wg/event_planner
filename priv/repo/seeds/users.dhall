let Data = { displayName : Text, email : Text, node_id : Text, photo : Text }

in    [ { displayName = "Stéphanie Da Cunha"
        , email = "s.dacunha@crans.org"
        , node_id = "ens-paris-saclay"
        , photo = "https://assets.crans.org/s.dacunha/photo.jpg"
        }
      , { displayName = "Guðfinna Sigfúsdóttir"
        , email = "gudhfinna.sigfusdottir@rezel.net"
        , node_id = "telecom-paris"
        , photo = "https///assets.rezel.net/gudhfinna.sigfusdottir/photo.jpg"
        }
      , { displayName = "Lạc Thị Cúc Gấm"
        , email = "lac.thi.cuc.gam@data-ensta.fr"
        , node_id = "ensta"
        , photo = "https://users.data-ensta.fr/lac.thi.cuc.gam/photo.jpg"
        }
      , { displayName = "Praxidike Meng"
        , email = "p.meng@rmd-southern.ganyme.de"
        , node_id = "centrale-supelec"
        , photo = "https://assets.linkcs.fr/p.meng/photo.jpg"
        }
      , { displayName = "Olivér Facklam"
        , email = "olivér.facklam@polytechnique.edu"
        , node_id = "polytechnique"
        , photo = "https://assets.sigma.lol/olivér.facklam/photo.jpg"
        }
      , { displayName = "Wen Liu"
        , email = "wen.liu@institutoptique.fr"
        , node_id = "supoptique"
        , photo = "https://iotarie.institutoptique.fr/photos/wen.liu/photo.jpg"
        }
      ]
    : List Data
