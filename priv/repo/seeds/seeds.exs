alias EventPlanner.PartyLine
alias EventPlanner.PartyLine.Author

users = "priv/repo/seeds/users.json"
          |> File.read!()
          |> Jason.decode!(keys: :atoms)
          |> Enum.map(&PartyLine.create_user!(&1))


authors = "priv/repo/seeds/authors.json"
          |> File.read!()
          |> Jason.decode!(keys: :atoms)

### Helpers
find_uuid_by_email = fn email -> Enum.flat_map(users, fn u ->
                                                          case u.email == email do
                                                            true  -> [u.uuid]
                                                            false -> []
                                                          end
                                                        end) |> hd
                                                      end


find_user_by_email = fn email -> Enum.flat_map(users, fn u -> 
                                    case u.email == email do
                                      true  -> [u]
                                      false -> []
                                    end
                                  end) |> hd
                               end

### Posts
posts = "priv/repo/seeds/posts.json"
            |> File.read!()
            |> Jason.decode!(keys: :atoms)
            |> Enum.map(&PartyLine.create_post!(find_user_by_email.(&1.author.email), &1))

### Events
"priv/repo/seeds/events.json"
  |> File.read!()
  |> Jason.decode!(keys: :atoms)
  |> Enum.map(&PartyLine.create_event!(find_user_by_email.(&1.author.email), &1))


### Post and Comments
%{post: post, comments: comments, subcomment: subcomment} =
  "priv/repo/seeds/post_and_comments.json"
  |> File.read!()
  |> Jason.decode!(keys: :atoms)

post = PartyLine.create_post!(find_user_by_email.(post.author.email), post)

results =
  comments |> Enum.map(&PartyLine.create_response(%{parent: post, user: find_user_by_email.(&1.author.email), 
                                                    attrs: &1, to: :post
                                                  }))

comment1  = Enum.filter(results, fn c -> c.attachments == ["https://i.imgur.com/D1nITb9.png"] end) |> hd
PartyLine.create_response(%{parent: comment1, user: find_user_by_email.(subcomment.author.email), attrs: subcomment, to: :post})


### Event and Comments

%{event: event, comments: event_comments, subcomments: subcomments, subsubcomments: subsubcomments} =
  "priv/repo/seeds/event_and_comments.json"
  |> File.read!()
  |> Jason.decode!(keys: :atoms)

created_event = PartyLine.create_event!(find_user_by_email.(event.author.email), event)

[c1|_] =
  event_comments
  |> Enum.map(&PartyLine.create_response(%{parent: created_event, user: find_user_by_email.(&1.author.email), attrs: &1, to: :event}))

[s1] = 
  subcomments
  |> Enum.map(&PartyLine.create_response(%{parent: c1, user: find_user_by_email.(&1.author.email), attrs: &1, to: :post}))

[ss1] = 
  subsubcomments
  |> Enum.map(&PartyLine.create_response(%{parent: s1, user: find_user_by_email.(&1.author.email), attrs: &1, to: :post}))
