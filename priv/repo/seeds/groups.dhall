let Types = ./types.dhall

in    [ { displayName =
            "WACS"
        , email = "br@binets.fr"
        , photo =
            "https://ensta-data.fr/saturne/group/4d7e1c58-98e7-4c02-aeac-d1c0407d7cb9"
        , node_id = "polytechnique"
        }
      , { displayName = "Rezel"
        , email = "contact@rezel.net"
        , photo = "https://tutos.apps.rezel.net/logo.png"
        , node_id = "telecom-paristech"
        }
      ]
    : List Types.Group
