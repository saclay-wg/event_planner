let Types = ./types.dhall

let author1 =
        { id = "00000000-0000-0000-0000-000000000001"
        , email = "olivér.facklam@polytechnique.edu"
        , node_id = "polytechnique"
        , type = "user"
        }
      : Types.Author

let author2 =
        { id = "00000000-0000-0000-0000-000000000004"
        , node_id = "telecom-paris"
        , email = "gudhfinna.sigfusdottir@rezel.net"
        , type = "group"
        }
      : Types.Author


let author3 =
        { id = "00000000-0000-0000-0000-000000000005"
        , email = "lac.thi.cuc.gam@data-ensta.fr"
        , node_id = "ensta"
        , type = "user"
        }
      : Types.Author

in    [ { title =
            "Pizza + tarte en OPEN"
        , description =
            "Venez vous servir au grand Hall, il reste plein de pizza, tartes au pomme et jus de fruit provenant du forum 4A !"
        , createdAt = "2019-03-16T23:08:58.109Z"
        , updatedAt = "2019-03-16T23:08:58.109Z"
        , node_id = "polytechnique"
        , author = author1
        , attachments =
            [ "https://sigma.binets.fr/assets/8c1c6032-42ea-455a-a4f5-c58ee7eaced2"
            ]
        }
      , { title =
            "Un jour une loutre"
        , description = "Tous les jours, une loutre"
        , createdAt = "2019-03-16T23:08:58.109Z"
        , updatedAt = "2019-03-16T23:08:58.109Z"
        , node_id = "ensta"
        , author = author2
        , attachments =
            [ "https://cdn.pixabay.com/photo/2016/06/05/22/42/otter-1438378_1280.jpg"
            ]
        }
      , { title = "Mèmes mathématiques machiavéliques"
        , description = "Allons au barbershop"
        , createdAt = "2019-03-16T23:08:58.109Z"
        , updatedAt = "2019-03-16T23:08:58.109Z"
        , node_id = "ensta"
        , author = author3
        , attachments = [ "https://i.chzbgr.com/full/9178822912/hADB531D8" ]
        }
      ]
    : List Types.Post
