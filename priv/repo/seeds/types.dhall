-- Different from the spec's Author model.
let Author = { id : Text, node_id : Text, email : Text, type : Text }

let Event =
      { startTime : Text
      , endTime : Text
      , author : Author
      , title : Text
      , place : Text
      , node_id : Text
      , description : Text
      }

let Group = { displayName : Text, email : Text, photo : Text, node_id : Text }

let Post =
      { title : Text
      , description : Text
      , createdAt : Text
      , updatedAt : Text
      , node_id : Text
      , author : Author
      , attachments : List Text
      }

let User = { displayName : Text, email : Text, photo : Text, node_id : Text }

in  { Author = Author
    , Event = Event
    , Group = Group
    , Post = Post
    , User = User
    }
