let Types = ./types.dhall

let author1 =
        { id = "00000000-0000-0000-0000-000000000001"
        , node_id = "polytechnique"
        , email = "olivér.facklam@polytechnique.edu"
        , type = "user"
        }
      : Types.Author

let author2 =
        { id = "00000000-0000-0000-0000-000000000002"
        , node_id = "centrale-supelec"
        , email = "s.dacunha@crans.org"
        , type = "user"
        }
      : Types.Author

let startTime1 = "2019-04-13T14:25:25.178Z"

let endTime1 = "2019-04-13T16:25:25.178Z"

let author1 = author1

let title1 = "Soirée ramassage de déchets"

let place1 = "828 Boulevard des Maréchaux, 91120 Palaiseau, France"

let node_id1 = "polytechnique"

let description1 = "À vos sacs poubelles ! Prêt⋅e⋅s ! Partez !!"

let startTime2 = "2019-04-13T17:58:16.705Z"

let endTime2 = "2019-04-13T19:58:16.705Z"

let author2 = author2

let title2 = "Große Binouze Franco-Allemande"

let place2 = "9 rue Joliot Curie, 91190 Gif-sur-Yvette"

let node_id2 = "centrale-supelec"

let description2 =
      "Venons célebrer l'amitiée Franco-Allemande avec les étudiant⋅e⋅s d'Outre-Rhin en résidence à CentraleSupélec !"

in    [ { startTime = startTime1
        , endTime = endTime1
        , author = author1
        , title = title1
        , node_id = node_id1
        , place = place1
        , description = description1
        }
      , { startTime = startTime2
        , endTime = endTime2
        , author = author2
        , title = title2
        , node_id = node_id2
        , place = place2
        , description = description2
        }
      ]
    : List Types.Event
