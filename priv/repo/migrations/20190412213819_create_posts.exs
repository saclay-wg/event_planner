defmodule EventPlanner.Repo.Migrations.CreatePosts do
  use Ecto.Migration


  def change do
    create table(:posts, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :title, :string
      add :node_id, :string, null: false
      add :attachments, {:array, :string}, null: false
      add :description, :string, null: false
      add :hash, :string
      add :author, :map, null: false

      add :user_id, references(:users, type: :uuid)
      add :post_id, references(:posts, type: :uuid)
      add :event_id, references(:events, type: :uuid)

      timestamps(inserted_at: :created_at)
    end

    create index(:posts, [:title, :hash], [unique: true])
  end
end
