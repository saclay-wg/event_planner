defmodule EventPlanner.Repo.Migrations.CreateEvents do
  use Ecto.Migration

  def change do
    create table(:events, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :description, :string, null: false
      add :endTime, :utc_datetime, null: false
      add :startTime, :utc_datetime, null: false
      add :place, :string, null: false
      add :title, :string, null: false
      add :hash, :string
      add :author, :map, null: false
      add :node_id, :string, null: false

      add :user_id, references(:users, type: :uuid)

      timestamps(inserted_at: :created_at)
    end

    create index(:events, :id, [unique: true])
    create index(:events, :hash, [unique: true])
    create index(:events, [:title, :startTime], [unique: true])

  end
end
