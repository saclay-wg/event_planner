defmodule EventPlanner.Repo.Migrations.CreateUsers do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :displayName, :string
      add :email, :string
      add :photo, :string
      add :node_id, :string

      timestamps(inserted_at: :created_at)
    end

    create index(:users, [:email], [unique: true])

  end
end
