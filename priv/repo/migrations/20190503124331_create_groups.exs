defmodule EventPlanner.Repo.Migrations.CreateGroups do
  use Ecto.Migration

  def change do
    create table(:groups, primary_key: false) do
      add :uuid, :binary_id, primary_key: true
      add :displayName, :string
      add :email, :string
      add :photo, :string
      add :node_id, :string

      timestamps()
    end

  end
end
