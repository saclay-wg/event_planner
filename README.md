# EventPlanner

EventPlanner is a prototype / proof of concept implementation of the PartyLine
API specification. The system stores posts, events, users and groups that are
present on the Saclay campus. It is currently in development mode, and you
can follow the issues list to see what has been implemented.

You can also chat with the developers [on Telegram](https://t.me/saclaywg)!

## Development requirements

>To install Elixir and Erlang, use [asdf](https://github.com/asdf-vm/asdf).

* Erlang/OTP ≥ 21
* Elixir ≥ 1.9
* PostgreSQL ≥ 11

## Settings up the project and starting it

* Install dependencies with `make build`
* Create, migrate and seed your database with `make reset && make seed`
* Start the server with `make start`

## Configuring PostgreSQL

1. Fetch the configuration file with `psql -U postgres -c "SHOW hba_file"`
   (provided that you have a `postgres` user)
1. Edit the bottom of the file so it looks like this

```
# TYPE  DATABASE        USER            ADDRESS                 METHOD
# "local" is for Unix domain socket connections only
local   all             all                                     peer
# IPv4 local connections:
host    all             all             127.0.0.1/32            md5
# IPv6 local connections:
host    all             all             ::1/128                 md5
```

## Commands to try in the Elixir REPL

You can use helpers available in the REPL to get all the posts and users.
From that, you can extract their UUID and insert it in `curl`/`httpie` commands
in your terminal or API client.

### Fetching a user from the system

```elixir
iex(1)> list_users |> hd |> Map.get(:uuid)

"92040601-02c9-4f58-9efb-30931a874191"
```

```bash
❯ http localhost:4000/api/user/92040601-02c9-4f58-9efb-30931a874191
```
<details>
<summary>response</summary>

```json
{
    "displayName": "Stéphanie Da Cunha",
    "email": "s.dacunha@crans.org",
    "nodeId": "ens-cachan",
    "photo": "https://assets.crans.org/92040601-02c9-4f58-9efb-30931a874191/photo.jpg",
    "uuid": "92040601-02c9-4f58-9efb-30931a874191"
}
```

</details>

### Fetching a post from the system

```elixir
iex(1)> list_users |> hd |> Map.get(:uuid)

"1c349beb-9c12-4b95-809e-817b71f3ec0a"
```

```bash
❯ http localhost:4000/api/posts/92040601-02c9-4f58-9efb-30931a874191
```

<details>
<summary>response</summary>

```JSON
{
  "attachments": [],
  "author": {
    "id": "2ce541b8-b993-4ffa-8e6e-b9e7194e559a",
    "nodeId": "telecom-paristech",
    "type": "user"
  },
  "children": [
    {
      "attachments": [],
      "author_id": "a844b393-76a5-4347-a374-96a349a7b21e",
      "authors_node_id": "ensta",
      "author_type": "user",
      "created_at": "2019-06-06T15:56:16",
      "updated_at": "2019-06-06T15:56:16",
      "description": "Non, désolé :/",
      "node_id": "polytechnique",
      "uuid": "92fef291-5eff-4092-9c2a-21b16c72c9c6",
      "title": null,
      "children": []
    },
    {
      "attachments": [
        "https://i.imgur.com/D1nITb9.png"
      ],
      "author_id": "92040601-02c9-4f58-9efb-30931a874191",
      "authors_node_id": "ens-cachan",
      "author_type": "user",
      "created_at": "2019-06-06T15:56:16",
      "updated_at": "2019-06-06T15:56:16",
      "description": "Est-ce qu'il est bleu et blanc ? J'en ai vu un traîner vers l'arrêt de bus",
      "node_id": "polytechnique",
      "uuid": "3e234a95-33c2-4fdc-a221-29c88eb795ad",
      "title": null,
      "children": [
        {
          "attachments": [
            "https://i.imgur.com/dfoj4AD.jpg"
          ],
          "author_id": "2ce541b8-b993-4ffa-8e6e-b9e7194e559a",
          "authors_node_id": "telecom-paristech",
          "author_type": "user",
          "created_at": "2019-06-06T15:56:16",
          "updated_at": "2019-06-06T15:56:16",
          "description": "Merciiiii ❤️",
          "node_id": "polytechnique",
          "uuid": "9d9ffe25-b0b4-4433-b804-e9b9ac20eca6",
          "title": null,
          "children": []
        }
      ]
    }
  ],
  "createdAt": "2019-06-06T15:56:16",
  "description": "J'ai perdu mon collier de perles dans le parc près de l'X…\nEst-ce que quelqu'un l'aurait vu ?\n",
  "nodeId": "polytechnique",
  "parent": null,
  "title": "Est-ce que quelqu'un a vu un collier de perles ?",
  "updatedAt": "2019-06-06T15:56:16",
  "uuid": "1c349beb-9c12-4b95-809e-817b71f3ec0a"
}
```

</details>


## Notes

- Tests do not work.

## Misc

### Dhall

The database's seed files are written in [Dhall](https://dhall-lang.org).
This configuration language allows for static type checking, an expressive syntax
and transpiles to JSON and YAML easily.

You can see the differences between [dhall](./priv/repo/posts.dhall) and [json](./priv/repo/posts.json).

### Nix

For reproducible builds across machines, this project has a `shell.nix` file.
It is used by the [Nix](https://nixos.org/nix) package manager to be able to
bootstrap a complete development environment with ease of reproducibility.

You need to install Nix to be able to use it, and then enter in the Nix
environment with `nix-shell [--pure] shell.nix`.

If you wish to stay in your own shell environment and yet leverage the power
of Nix, all the Makefile commands to reset the database, build and
start the project have `nix-` variants. They will call Nix on your behalf.

[Here is a talk (in french) on Nix](https://www.youtube.com/watch?v=v0NY2VNcoWU).

## Where do I learn all that stuff

* [The Elixir Documentation](https://hexdocs.pm/elixir/Kernel.html)
* [The Elixir Guides](https://elixir-lang.org/getting-started/introduction.html)
* [The Unofficial NixOS Wiki](https://nixos.wiki/wiki/Resources)
* [Dhall tutorial to generate JSON or YAML](https://github.com/dhall-lang/dhall-lang/wiki/Getting-started%3A-Generate-JSON-or-YAML)
